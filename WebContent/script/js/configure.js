$('document').ready(function(){

	magicActiveNavLink('empty');
	
	if (sessionStorage.getItem("activeConfigureTab")!=null){
		showActiveConfigureTab(sessionStorage.getItem("activeConfigureTab"));
		sessionStorage.removeItem("activeConfigureTab")
	}
});

function showActiveConfigureTab(activeTab){

	$('#ulNavTab li').each(function(){
		$(this).removeClass('active');
		if($(this).text()==activeTab){
			$(this).addClass('active');
		}
	});

	$('#divCenter, #divGrade, #divProgramme').addClass('hide');
	$('#divNavTabs div').each(function(){
		if($(this).prop('id')=='div'+activeTab){
			$(this).removeClass('hide');
		}
	});
	
	if($('#divCenter').is(":visible")){
		getCenterDetails();
	}else if($('#divGrade').is(":visible")){
		getGradeDetails();
	}else if($('#divProgramme').is(":visible")){
		getProgrammeDetails();
	}
}

$('#modelAddEdit').on('shown.bs.modal',function(event){

	var button = $(event.relatedTarget);
	var callerBtn = button.data('callerbtn');
	
	//$(this).find('.modal-dialog').addClass('modal-sm');
	if(callerBtn.split('-')[0]=='Center'){
		$(this).find('.modal-body').load('addEditCenter.io');
	}
	else if(callerBtn.split('-')[0]=='Grade'){
		$(this).find('.modal-body').load('addEditGrade.io');
	}
	else if(callerBtn.split('-')[0]=='Programme'){
		$(this).find('.modal-body').load('addEditProgramme.io');
	}
	
	if(callerBtn.split('-')[1]=='Add'){
		$('#btnAddEditModel').text('Save');
	}
	else if(callerBtn.split('-')[1]=='Edit'){
		$('#btnAddEditModel').text('Edit');
		sessionStorage.setItem("editConfiguration",callerBtn.split('-')[2]);
	}
	
	$('#modelAddEditHeader').text(callerBtn.split('-')[1]+' '+callerBtn.split('-')[0]);
	
});


function getCenterDetails(){

	$.ajax({
		url:'getCenterDetails.io',
		type:'GET',
		dataType:'json',
		beforeSend:function(){
			magicStartBlur();
		},
		success:function(jsonArray){
			var str='<table id="tblCenter" class="table table-condensed table-hover" style="text-align: center;">'+
			'<thead><tr><th>#</th><th>Name</th><th>Location</th><th>Conatct</th><th>Action</th></tr></thead>'
			
			str+='<tbody>';
			var i=0;
			var centerId=0;
			$.each(jsonArray, function() {
				str+='<tr>';
				$.each(this, function(key, value) {
					if(key=='centerId'){
						str+='<td>'+parseInt(++i)+'</td>';
						centerId=value;
					}	
					else
						str+='<td>'+value+'</td>';
				});
				str+='<td>'+
						'<span data-toggle="modal" data-target="#modelAddEdit" data-backdrop="static" data-callerbtn="Center-Edit-'+centerId+'">'+
						'<button type="button" class="btn btn-link" data-toggle="tooltip" data-placement="left" title="Edit">'+
						'<span class="glyphicon glyphicon-edit"></span></button></span>'+
						
						'<button type="button" class="btn btn-link" data-toggle="tooltip" data-placement="right" title="Delete" '+
						'onclick="deleteCenter('+centerId+')"><span class="glyphicon glyphicon-trash"></span></button>'+
					 '</td>';
				str+='</tr>';
			});
			str+='</tbody>';
			
			str+='</table>';
			$('#divCenterList').html(str).parent().removeClass('hide');
			$('#tblCenter').DataTable();
			$('[data-toggle="tooltip"]').tooltip();
		},
		error:function(){
			magicError('Calling "getCenterDetails.io" ');
		},
		complete:function(){
			magicStopBlur();
		},
	});
}

function deleteCenter(centerId){

	/*jAlert('This is a custom alert box', 'Alert Dialog'); */
return;
	/*$.ajax({
		url:'deleteCenter.io',
		type:'POST',
		data:{centerId:centerId},
		dataType:'text',
		beforeSend:function(){
			magicStartBlur();
		},
		success:function(msg){
			getCenterDetails();
		},
		error:function(){
			magicError('Calling "deleteCenter.io" ');
		},
		complete:function(){
			magicStopBlur();
		},
	});*/
}





