

$('#loginForm').on('submit',function(e){
	e.preventDefault();
	
	var login=$('#inputEmail').val();
	var password=$('#inputPassword').val();
	
	$.ajax({
		url:'userLogin.io',
		data:{login:login,password:password},
		type:'POST',
		dataType:'text',
		async:false,
		beforeSend:function(){
			magicStartBlur();
			$('#buttonSignIn').prop('disabled',true);
		},
		success:function(result){
			if(result=='Success')
				window.location.href = 'Home.io';
			else
				magicError('Incorrect Username or Password');
		},
		error:function(){
			magicError('Calling "loginUser.io" ');
		},
		complete:function(){
			$('#buttonSignIn').prop('disabled',false);
			magicStopBlur();
		},
	})
});

