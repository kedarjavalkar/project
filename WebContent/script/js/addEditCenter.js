
$('document').ready(function() {

	$('#btnAddEditModel').bind('click', function() {
		$('#btnCenterAddEdit').click();
	});
	
	if(sessionStorage.getItem("editConfiguration")!=null){
		editCenterDetails(sessionStorage.getItem("editConfiguration"));
		sessionStorage.removeItem("editConfiguration")
	}

	$('#formCenterAddEdit').validator().on('submit', function(e) {
		if (e.isDefaultPrevented()) {
			// do something when error
		} else {
			e.preventDefault(); //preventing form submit
			saveOrUpdateCenter();
		}
	});
	
});

function editCenterDetails(centerId){
	
	$.ajax({
		url:'editCenterDetails.io',
		data:{centerId:centerId},
		type:'GET',
		dataType:'json',
		beforeSend:function() {
			magicStartBlur();
		},
		success:function(json) {
			$('#formCenterAddEdit').data('centerId',json['centerId']);
			$('#inputName').val(json['name']);
			$('#textAreaLocation').val(json['location']);
			$('#inputContact').val(json['contactNo']);
		},
		error:function() {
			magicError('Calling "editCenterDetails.io" ');
		},
		complete:function() {
			magicStopBlur();
		},
	});
}

function saveOrUpdateCenter() {

	var centerId = $('#formCenterAddEdit').data('centerId');
	var name = $('#inputName').val().trim();
	var location = $('#textAreaLocation').val().trim();
	var contact= $('#inputContact').val().trim();
	
	$.ajax({
		url:'saveOrUpdateCenter.io',
		data:{centerId:centerId,name:name,location:location,contact:contact},
		type:'POST',
		dataType:'text',
		beforeSend:function() {
			magicStartBlur();
		},
		success:function(msg) {
			$('#modelAddEdit').modal('hide');
			getCenterDetails();
			magicSuccess("Center deatils are stored");
		},
		error:function() {
			magicError('Calling "saveOrUpdateCenter.io" ');
		},
		complete:function() {
			magicStopBlur();
		},
	});
}

