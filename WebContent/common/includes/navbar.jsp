

<!-- Navbar Start -->
<div class="container-fluid">
	<div class="navbar navbar-inverse" role="navigation">
	
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="javascript:void(0)">#Project</a>
		</div>
		
		<div class="navbar-collapse collapse" id="divNavBar">
			<ul class="nav navbar-nav">
				<li><a href="Home.io"><span class="glyphicon glyphicon-home"></span> Home</a></li>
				<!-- <li><a href="#about">About</a></li> -->
			</ul>
			
			<ul class="nav navbar-nav navbar-right">
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Configure
						<span class="caret"></span></a>
					<ul class="dropdown-menu" role="menu">
						<li onclick="setActiveConfigureTab(this)"><a href="Configure.io">Center</a></li>
						<li onclick="setActiveConfigureTab(this)"><a href="Configure.io">Grade</a></li>
						<li onclick="setActiveConfigureTab(this)"><a href="Configure.io">Programme</a></li>
						<li class="divider"></li>
						<li><a href="#">Batch</a></li>
					</ul></li>
				<li><a href="Login.io"><span class="glyphicon glyphicon-off"></span> Logoff</a></li>
			</ul>
		</div> <!--/.nav-collapse -->
		
	</div>
</div> 
 <!-- Navbar End -->

<script type="text/javascript">
	function setActiveConfigureTab(eThis){
		sessionStorage.setItem("activeConfigureTab",$(eThis).text());
	}
</script>



