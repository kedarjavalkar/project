<!DOCTYPE html >
<html lang="en">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<jsp:include page="/common/includes/header.jsp"></jsp:include>
<title>Configure</title>
</head>
<body>
	<jsp:include page="/common/includes/navbar.jsp"></jsp:include>
	<div class="container-fluid" style="margin-bottom: 25px;">
		
		<ul class="nav nav-tabs" id="ulNavTab">
			<li role="presentation" onclick="showActiveConfigureTab('Center')"><a href="javascript:void(0)">Center</a></li>
			<li role="presentation" onclick="showActiveConfigureTab('Grade')"><a href="javascript:void(0)">Grade</a></li>
			<li role="presentation" onclick="showActiveConfigureTab('Programme')"><a href="javascript:void(0)">Programme</a></li>
		</ul>
		
		<div class="container-fluid" id="divNavTabs" style="margin-top: 10px;">
			<div id="divCenter" class="hide">
				<button type="button" class="btn btn-primary pull-left" data-toggle="modal" data-target="#modelAddEdit"
					data-backdrop="static" data-callerbtn="Center-Add">Add Center</button>
				<div class="panel panel-default float-right hide" style="width:70%; margin: 0 auto;">
					<div class="panel-heading"><h3 class="panel-title">Center List</h3></div>
					<div class="panel-body" id="divCenterList">Panel content</div>
				</div>
			</div>
			<div id="divGrade" class="hide">
				<button type="button" class="btn btn-primary pull-left" data-toggle="modal" data-target="#modelAddEdit"
					data-backdrop="static" data-callerbtn="Grade-Add">Add Grade</button>
			</div>
			<div id="divProgramme" class="hide">
				<button type="button" class="btn btn-primary pull-left" data-toggle="modal" data-target="#modelAddEdit"
					data-backdrop="static" data-callerbtn="Programme-Add">Add Programme</button>
			</div>
		</div>
		
	</div>
	
	<jsp:include page="/common/includes/footer.jsp"></jsp:include>
	<script type="text/javascript" src="script/js/configure.js"></script>
</body>
</html>