

<form role="form" data-toggle="validator" id="formCenterAddEdit">
	<table class="table table-condensed table-hover">
		<tr> <td> <div class="form-group">
					<label for="inputName" class="control-label">Center Name *</label>
					<input type="text" class="form-control" id="inputName" required>
					<div class="help-block with-errors"></div>
				</div> </td> </tr>
		<tr> <td> <div class="form-group">
					<label for="textAreaLocation" class="control-label">Location *</label>
					<textarea rows="3" cols="6" class="form-control" id="textAreaLocation" required></textarea>
					<div class="help-block with-errors"></div>
				</div> </td> </tr>
		<tr> <td> <div class="form-group">
					<label for="inputContact" class="control-label">Contact Number *</label>
					<input type="text" class="form-control" id="inputContact" required>
					<div class="help-block with-errors"></div>
				</div> </td> </tr>
		<tr style="display: none;"> <td> <div class="form-group">
    				<button type="submit" id="btnCenterAddEdit">Submit</button>
  				</div> </td> </tr>
  		<tr class="pull-right"><td>* <span style="font-size: 10px;">Required field</span></td></tr>
	</table>
</form>


<script src="script/js/addEditCenter.js"></script>

