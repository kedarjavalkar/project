package project.controller.center;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import project.bi.center.CenterManager;
import project.entity.Center;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

@Controller
public class CenterController {

	@Autowired
	CenterManager centerManager;

	@RequestMapping(value="getCenterDetails.io",method=RequestMethod.GET)
	@ResponseBody String getCenterDetails(HttpServletRequest request){

		List<Center> centerList = centerManager.getAllCenter();
		JsonArray jsonArray = new JsonArray();
		for (Center center : centerList) {
			JsonObject jsonObj = new JsonObject();
			jsonObj.addProperty("centerId", center.getId());
			jsonObj.addProperty("name", center.getName());
			jsonObj.addProperty("location", center.getLocation());
			jsonObj.addProperty("contactNo", center.getContactNo());
			jsonArray.add(jsonObj);
		}

		return new Gson().toJson(jsonArray);
	}

	@RequestMapping(value="saveOrUpdateCenter.io",method=RequestMethod.POST)
	@ResponseBody String saveOrUpdateCenter(HttpServletRequest request){

		String name = request.getParameter("name");
		String location = request.getParameter("location");
		String contact = request.getParameter("contact");
		String centerId = request.getParameter("centerId");
		
		Center centerObj = new Center();
		centerObj.setName(name);
		centerObj.setLocation(location);
		centerObj.setContactNo(contact);
		centerObj.setActive(true);
		if(centerId!=null)
			centerObj.setId(Integer.parseInt(centerId));
		
		return centerManager.saveOrUpdate(centerObj);
	}
	
	@RequestMapping(value="editCenterDetails.io",method=RequestMethod.GET)
	@ResponseBody String editCenterDetails(HttpServletRequest request){
		
		Center centerObj = null;
		centerObj = centerManager.getCenterById(Integer.parseInt(request.getParameter("centerId")));
		JsonObject jsonObj = new JsonObject();
		jsonObj.addProperty("centerId", centerObj.getId());
		jsonObj.addProperty("name", centerObj.getName());
		jsonObj.addProperty("location", centerObj.getLocation());
		jsonObj.addProperty("contactNo", centerObj.getContactNo());
		
		return new Gson().toJson(jsonObj);
	}
	
	@RequestMapping(value="deleteCenter.io",method=RequestMethod.POST)
	@ResponseBody String deleteCenter(HttpServletRequest request){

		String centerId = request.getParameter("centerId");
		Center centerObj = new Center();
		centerObj.setActive(false);
		centerObj.setId(Integer.parseInt(centerId));
		System.out.println(centerObj.getLocation());
		System.out.println(centerObj.getId());
		
		return centerManager.saveOrUpdate(centerObj);
		
	}
	
	
}
