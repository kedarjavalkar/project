package project.controller.login;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import project.bi.user.UserManager;


@Controller
public class UserController {

	@Autowired
	private UserManager userManager;

	@RequestMapping(value="userLogin.io",method=RequestMethod.POST)
	@ResponseBody String loginUser(HttpServletRequest request){

		String login = request.getParameter("login");
		String password = request.getParameter("password");

		if(userManager.checkUserLogin(login, password)){
			//HttpSession session = request.getSession();
			//session.setAttribute("ProjectUserName", login);
			return "Success";
		}
		return "Failure";
	}

}
