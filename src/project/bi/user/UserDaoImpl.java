package project.bi.user;

import javax.transaction.Transactional;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import project.entity.User;

@Transactional
@Repository("UserDao")
public class UserDaoImpl implements UserDao{

	@Autowired
	SessionFactory sessionFactory;
	
	@Override
	public boolean checkUserLogin(String login, String password){
		
		User userObj = null;
		Session session = sessionFactory.openSession();
		userObj = (User) session.createCriteria(User.class)
							.add(Restrictions.eq("loginName", login))
							.add(Restrictions.eq("password", password))
							.add(Restrictions.eq("isActive", true))
							.list().get(0);

		/*Criteria criteria = session.createCriteria(User.class);
		criteria.add(Restrictions.eq("loginName", login));
		criteria.add(Restrictions.eq("password", password));
		criteria.setFirstResult(0);
		criteria.setMaxResults(1);
		userObj = (User) criteria.list().get(0);*/
		
		session.close();
		
		if(userObj==null){
			return false;
		}
		return true;
	}
	
}
