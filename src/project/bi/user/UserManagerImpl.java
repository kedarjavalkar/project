package project.bi.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service("UserManager")
public class UserManagerImpl implements UserManager {

	@Autowired
	UserDao userDao;
	
	@Override
	public boolean checkUserLogin(String login, String password){
		return userDao.checkUserLogin(login, password);
	}
	
}
