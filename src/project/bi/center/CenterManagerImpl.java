package project.bi.center;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import project.entity.Center;

@Service("CenterManager")
public class CenterManagerImpl implements CenterManager{

	@Autowired
	CenterDao centerDao;
	
	@Override
	public List<Center> getAllCenter(){
		return centerDao.getAllCenter();
	}
	@Override
	public String saveOrUpdate(Center centerObj){
		return centerDao.saveOrUpdate(centerObj);
	}
	@Override
	public Center getCenterById(Integer centerId){
		return centerDao.getCenterById(centerId);
	}
	
}
