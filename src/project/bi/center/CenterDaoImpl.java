package project.bi.center;

import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import project.entity.Center;

@Transactional
@Repository("CenterDao")
public class CenterDaoImpl implements CenterDao{

	@Autowired
	SessionFactory sessionFactory;
	
	@Override
	@SuppressWarnings("unchecked")
	public List<Center> getAllCenter(){
		
		List<Center> centerList=null;
		Session session = sessionFactory.openSession();
		centerList = session.createCriteria(Center.class)
						.add(Restrictions.eq("isActive", true)).list();
		session.close();

		return centerList;
	}
	
	@Override
	public String saveOrUpdate(Center centerObj){

		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		session.saveOrUpdate(centerObj);
		tx.commit();
		session.close();

		return "Success";
	}

	@Override
	public Center getCenterById(Integer centerId){
		
		Center centerObj = null;
		Session session = sessionFactory.openSession();
		centerObj = (Center) session.createCriteria(Center.class)
						.add(Restrictions.eq("id", centerId)).list().get(0);
		session.close();
		
		return centerObj;
	}
	
}
