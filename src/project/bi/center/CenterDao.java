package project.bi.center;

import java.util.List;

import project.entity.Center;

public interface CenterDao {

	public List<Center> getAllCenter();
	public String saveOrUpdate(Center centerObj);
	public Center getCenterById(Integer centerId);
}
