package project.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="grade_t")
public class Grade implements Serializable{

	private static final long serialVersionUID = 1628609918578853627L;

	@Id
	@GeneratedValue
	@Column(name="grade_id")
	private Integer id;
	
	@Column(name="value", length=25, nullable = false)
	private String value;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	
	
}
