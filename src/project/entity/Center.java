package project.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="center_t")
public class Center implements Serializable{

	private static final long serialVersionUID = -8278991733941175796L;
	
	@Id
	@GeneratedValue
	@Column(name="center_id")
	private Integer id;
	
	@Column(name="name", length=25, nullable = false)
	private String name;
	
	@Column(name="location", nullable = false)
	private String location;
	
	@Column(name="contact_no", length=25)
	private String contactNo;
	
	@Column(name="is_active")
	private boolean isActive;

	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getContactNo() {
		return contactNo;
	}

	public void setContactNo(String contactNo) {
		this.contactNo = contactNo;
	}
	
	public boolean isActive() {
		return isActive;
	}

	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}
	
}
