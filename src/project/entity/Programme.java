package project.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="programme_t")
public class Programme implements Serializable{

	private static final long serialVersionUID = 6292468384531235020L;

	@Id
	@GeneratedValue
	@Column(name="programme_id")
	private Integer id;
	
	@Column(name="name", length=25, nullable = false)
	private String name;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	
}
