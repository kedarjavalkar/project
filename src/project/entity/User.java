package project.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;



@Entity
@Table(name="user_t")
public class User implements Serializable {

	private static final long serialVersionUID = 1186350289761323695L;

	@Id
	@GeneratedValue
	@Column(name="user_id")
	private Integer Id;

	@Column(name="first_name", length=25, nullable = false)
	private String firstName;

	@Column(name="last_name", length=50, nullable = false)
	private String lastName;

	@Column(name="login_name", length=50)
	private String loginName;

	@Column(name="password", length=25)
	private String password;

	@Lob
	@Column(name="image", nullable=true, columnDefinition="mediumblob")
	private byte[] image;

	@Column(name="is_active")
	private Boolean isActive;

	/**************** Getters and Setter **********************************/	

	public Integer getId() {
		return Id;
	}

	public void setId(Integer id) {
		Id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getLoginName() {
		return loginName;
	}

	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	public byte[] getImage() {
		return image;
	}

	public void setImage(byte[] image) {
		this.image = image;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

}
