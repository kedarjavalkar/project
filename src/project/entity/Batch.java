package project.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name="batch_t")
public class Batch implements Serializable{

	private static final long serialVersionUID = 1744027136947029936L;

	@Id
	@GeneratedValue
	@Column(name="batch_id")
	private Integer id;
	
	@Column(name="start_from", nullable = false)
	private Date startFrom;
	
	@Column(name="end_till", nullable = false)
	private Date endTill;
	
	@ManyToOne
    @JoinColumn(referencedColumnName="center_id", name="center_id")
    private Center center;
	
	@ManyToOne
    @JoinColumn(referencedColumnName="grade_id", name="grade_id")
    private Grade grade;
	
	@ManyToOne
    @JoinColumn(referencedColumnName="programme_id", name="programme_id")
    private Programme programme;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getStartFrom() {
		return startFrom;
	}

	public void setStartFrom(Date startFrom) {
		this.startFrom = startFrom;
	}

	public Date getEndTill() {
		return endTill;
	}

	public void setEndTill(Date endTill) {
		this.endTill = endTill;
	}

	public Center getCenter() {
		return center;
	}

	public void setCenter(Center center) {
		this.center = center;
	}

	public Grade getGrade() {
		return grade;
	}

	public void setGrade(Grade grade) {
		this.grade = grade;
	}

	public Programme getProgramme() {
		return programme;
	}

	public void setProgramme(Programme programme) {
		this.programme = programme;
	}
	
	
	
}
